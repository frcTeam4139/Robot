

package org.usfirst.frc.team4139.robot;

import java.util.LinkedList;

import edu.wpi.first.wpilibj.*;
import org.usfirst.frc.team4139.robot.CAN.*;
import org.usfirst.frc.team4139.robot.Sensors.*;
import org.usfirst.frc.team4139.robot.Utils.*;

import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.vision.VisionThread;
import edu.wpi.first.wpilibj.networktables.NetworkTable;
//import grip.myGrip;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
    private LinkedList<Instruction> instructions;
    /*
     * Initialize variables here AUTO_MODE is a variable that corresponds to our
     * starting position. Facing the field from the pit, 1 is the left, 2 is the
     * center, 3 is the right.
     */
    private static int AUTO_MODE = 2;
    private CANWheels wheels;
    private CANClimber climber;
    public Controller stick;
    private Instruction currentInstruction;
    private Timer climbtimer;
//    private Compressor c;
//    private Solenoid sol;

    private static final int IMG_WIDTH = 320;
    private static final int IMG_HEIGHT = 240;

    private VisionThread visionThread;
    private double centerX = 0.0;
    private RobotDrive drive;

    private final Object imgLock = new Object();

    private NetworkTable networktable = NetworkTable.getTable("GRIP/myContoursReport");
    private boolean autoON = false;
    private Timer autoTimer;

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    @Override
    public void robotInit() {
//        UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
//        camera.setResolution(IMG_WIDTH, IMG_HEIGHT);
//
//        visionThread = new VisionThread(camera, new myGrip(), pipeline -> {
//            if (!pipeline.filterContoursOutput().isEmpty()) {
//                Rect r = Imgproc.boundingRect(pipeline.filterContoursOutput().get(0));
//                synchronized (imgLock) {
//                    centerX = r.x + (r.width / 2);
//                }
//            }
//        });
//        visionThread.start();

        // c = new Compressor(0);
        // CameraServer.getInstance().startAutomaticCapture();
        wheels = new CANWheels(4, 1, 3, 2);
        climber = new CANClimber(0);
    }

    @Override
    public void autonomousInit() {
        autoTimer = new Timer();
        autoTimer.start();

        autoON = true;
        /*
         * wheels.start(); currentInstruction = new NoInstruction();
         * instructions = new LinkedList<Instruction>();
         * 
         * switch(AUTO_MODE) { /* case 1: instructions.add(new
         * TimedDriveInstruction(wheels, 1.0)); instructions.add(new
         * TurnInstruction(wheels, 90, TurnDir.left)); instructions.add(new
         * TimedDriveInstruction(wheels, .75)); instructions.add(new
         * TurnInstruction(wheels, 90, TurnDir.right)); instructions.add(new
         * TimedDriveInstruction(wheels, .75)); instructions.add(new
         * TurnInstruction(wheels, 45, TurnDir.left)); instructions.add(new
         * TimedDriveInstruction(wheels, .75)); break;
         */ /*
             * case 2: instructions.add(new TimedSensorDriveInstruction(wheels,
             * 5.0)); instructions.add(new WaitInstruction(wheels,0.5));
             * instructions.add(new TimedDriveInstruction(wheels, 0.5)); break;
             * /* case 3: instructions.add(new TimedDriveInstruction(wheels,
             * 1.0)); instructions.add(new TurnInstruction(wheels, 90,
             * TurnDir.right)); instructions.add(new
             * TimedDriveInstruction(wheels, .75)); instructions.add(new
             * TurnInstruction(wheels, 90, TurnDir.left)); instructions.add(new
             * TimedDriveInstruction(wheels, .75)); instructions.add(new
             * TurnInstruction(wheels, 45, TurnDir.right)); instructions.add(new
             * TimedDriveInstruction(wheels, .75)); break;
             */
        // }
    }

    /**
     * This function is called periodically during autonomous
     */
    @Override
    // 1 second of timer is the equivalent of 4.777 feet or so we think so just
    // try that
    public void autonomousPeriodic() {

        // Compressor c = new Compressor(0);

        // c.setClosedLoopControl(true);
        // c.setClosedLoopControl(false);

        // boolean enabled = c.enabled();
        // boolean pressureSwitch = c.getPressureSwitchValue();
        // double current = c.getCompressorCurrent();

        /*
         * wheels.turnDegreeTest(90, TurnDir.right); Timer.delay(1);
         * wheels.turnDegreeTest(90, TurnDir.right); Timer.delay(2);
         * 
         * wheels.turnDegreeTest(30, TurnDir.right); wheels.turnDegreeTest(15,
         * TurnDir.left);
         */

        /*
         * double[] defaultValue = new double[0]; while(autoTimer.get() < 15.0)
         * { double[] xVals = networktable.getNumberArray("centerX",
         * defaultValue); if(xVals.length > 0){ if(Math.abs(xVals[0] - 80) < 10)
         * continue; else if(xVals[0] > 80) wheels.turn(-0.000005,
         * TurnDir.right);//0.000035(changed to negative?) else
         * wheels.turn(-0.000005, TurnDir.left);//0.000005 } }
         */

        if (autoTimer.get() < 2.0)// this does this
        {
            wheels.switchToTank();
             wheels.drive(.5, .5);
        }
        else {
        	wheels.drive(0, 0);
        }
        // wheels.turnDegreeTest(90, TurnDir.left);
        // Timer.delay(2);
    
    /*
     * while(autoTimer.get()<4.0)//this does this { wheels.switchToTank();
     * wheels.drive(.5, .5); }
     *  
     * while(autoTimer.get() >= 4.0 && autoTimer.get() < 6.0)//turn {
     * wheels.switchToTank(); wheels.drive(-0.5, 0.5); }
     * while(autoTimer.get()>=6.0) { wheels.drive(.5,.5); }
     * 
     * 
     * 
     * 
     * 
     * }
     * 
     * 
     * 
     * 
     * 
     * /* else if(xVals[0] > 80) wheels.drive(-.0005,
     * -.000004);//wheels.turn(-0.000005, TurnDir.right);//0.000035(changed to
     * negative?) else wheels.drive(-.000004, -.0005);//wheels.turn(-0.000005,
     * TurnDir.left);//0.000005
     * 
     */

    /*
     * gyro.Reset();
     * 
     * while (IsAutonomous()) { float angle = gyro.GetAngle(); // get heading
     * myRobot.Drive(-1.0, -angle * kP); // turn to correct heading Wait(0.004);
     * }
     */

    /*
     * double centerX; synchronized (imgLock) { centerX = this.centerX; } double
     * turn = centerX - (IMG_WIDTH / 2); System.out.println(turn);
     */

    /*
     * if(currentInstruction.execute(0.0)) { if(instructions.isEmpty())
     * currentInstruction = new NoInstruction(); else currentInstruction =
     * instructions.pop(); }
     */
     }

    @Override
    public void teleopInit() {
        climbtimer = new Timer();
        climbtimer.reset();
        climbtimer.start();
        wheels.start();
        wheels.switchToArcade();// arcade drive
        stick = new Controller();
        // c.setClosedLoopControl(true);
        stick.setArcade();// was tank
        currentInstruction = new NoInstruction();
        instructions = new LinkedList<Instruction>();
        autoON = false;

    }

    /**
     * This function is called periodically during operator control
     */
    @Override
    public void teleopPeriodic() {
        Compressor c = new Compressor(20);
        c.setClosedLoopControl(true);
        c.setClosedLoopControl(false);
        Solenoid sol = new Solenoid(7);
        c.start();
//        boolean enabled = c.enabled();
//        boolean pressureSwitch = c.getPressureSwitchValue();
//        double current = c.getCompressorCurrent();

        if (stick.getButtonA() && climbtimer.hasPeriodPassed(0.5)) {
            climber.toggle();
            climbtimer.reset();
        }
        if (stick.getButtonX())
            wheels.toggleDriveMode();
        wheels.drive(stick.getStickLeft(), stick.getStickRight());
        climber.climb();
        // 9/30/2017

        if (stick.getButtonB() && climbtimer.hasPeriodPassed(0.5)) {
            if (sol.get())
                sol.set(false);
            else
                sol.set(true);
        }

    }

    @Override
    public void testInit() {

    }

    /**
     * This function is called periodically during test mode
     */

}
